import { format } from "date-fns";
import styled from "styled-components";
import useGetProject from "../common/apollo/hooks/useGetProject";
import { parseMemberNames } from "../utils/parseMemberNames";
import Loading from "./Loading";

interface Props {
  fullPath: string;
  className?: string;
}
const ProjectDetailsContainer = styled.div`
  display: grid;
  gap: 24px;
  background-color: #eee;
  border-radius: 4px;
  padding: 24px;
`;

const StyledProjectHeading = styled.div`
  display: grid;
`;

const Label = styled.span`
  font-weight: bold;
`;

const ProjectDetails = ({ fullPath = "", className }: Props) => {
  const { data, loading } = useGetProject({
    variables: { fullPath },
  });
  const {
    project: {
      archived = null,
      createdAt = "",
      description = "",
      name = "",
      projectMembers = {},
      webUrl = "",
    } = {},
  } = data || {};

  const formattedDate = createdAt && format(new Date(createdAt), "MM/dd/yyyy");

  if (loading) {
    return <Loading />;
  }
  console.log(parseMemberNames(projectMembers));
  return (
    <ProjectDetailsContainer className={className}>
      <StyledProjectHeading>
        <h2>{name}</h2>
        <p>
          <Label>Status:</Label> {archived ? "Archived" : "Active"}
        </p>
        <p>
          <Label>Created:</Label> {formattedDate}
        </p>
      </StyledProjectHeading>
      <p>{description ? description : "No description provided"}</p>
      <p>
        <Label>Project Members:</Label>{" "}
        {parseMemberNames(projectMembers).join(", ")}{" "}
      </p>
      <a href={webUrl} target="_blank" rel="noreferrer">
        Visit Project
      </a>
    </ProjectDetailsContainer>
  );
};

export default styled(ProjectDetails)``;
