import styled from "styled-components";
interface Props {
  className?: string;
  selectedProject: (path: string) => void;
  searchResults: {
    projects: {
      nodes: {
        [propName: string]: any;
      }[];
    };
  };
}

const StyledName = styled.p`
  font-weight: bold;
  font-size: 20px;
`;

const SearchResultsContainer = styled.div`
  display: grid;
  gap: 24px;
  margin: 24px;
`;

const StyledResult = styled.div`
  padding: 24px;
  background-color: #efe;
  border: 4px solid #888;
  border-radius: 8px;

  :hover {
    box-shadow: 10px 10px 5px grey;
    transition: box-shadow 0.2s ease-in-out;
    cursor: pointer;
  }
`;
const SearchResults = ({
  searchResults,
  selectedProject,
  className,
}: Props) => {
  const projects = searchResults?.projects?.nodes;

  return (
    <SearchResultsContainer className={className}>
      {projects?.map(({ fullPath, name, description }) => {
        return (
          <StyledResult
            key={fullPath}
            onClick={() => selectedProject(fullPath)}
          >
            <StyledName>{name}</StyledName>
            <p>{description ? description : "No description provided"}</p>
          </StyledResult>
        );
      })}
    </SearchResultsContainer>
  );
};

export default styled(SearchResults)``;
