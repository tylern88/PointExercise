import { SyntheticEvent, useState } from "react";
import styled from "styled-components";
import useGetProjects from "../common/apollo/hooks/useGetProjects";
import SearchResults from "./SearchResults";
import ProjectDetails from "./ProjectDetails";
import Loading from "./Loading";
import NoContent from "./NoContent";

const StyledForm = styled.form``;

const StyledHeader = styled.h1``;

const SearchDetailsSection = styled.div`
  & > ${ProjectDetails} {
    margin-top: 24px;
  }
`;

const SearchContainer = styled.div`
  display: grid;
  gap: 16px;
  margin-top: 32px;
  justify-self: center;
`;

const StyledBackButton = styled.button`
  width: 100px;
  font-size: 16px;
  outline-style: none;
  text-decoration: none;
`;

const Container = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  justify-content: center;
  & > ${StyledForm} {
    justify-self: center;
  }
  & > ${StyledHeader} {
    justify-self: center;
  }
  & > ${SearchResults} {
    margin-top: 24px;
  }
  & > ${SearchDetailsSection} {
    margin: 24px;
  }
  & > ${Loading} {
    justify-self: center;
    align-self: center;
    margin: 32px;
  }
  & > ${NoContent} {
    justify-self: center;
    align-self: center;
    margin: 32px;
  }
`;

const Search = () => {
  const [shouldDisplayProjectDetails, setShouldDisplayProjectDetails] =
    useState(false);

  const [searchInput, setSearchInput] = useState("");

  const [selectedProjectPath, setSelectedProjectPath] = useState("");

  const [
    searchProjects,
    { data: searchResults, loading, called: hasGetProjectsBeenCalled },
  ] = useGetProjects();

  const handleChange = (event: SyntheticEvent) => {
    const value = (event.target as HTMLInputElement).value;
    setSearchInput(value);
  };

  const handleClearInput = () => setSearchInput("");

  const handleSubmit = (event: SyntheticEvent) => {
    event.preventDefault();
    if (!searchInput) return;
    setSelectedProjectPath("");
    if (shouldDisplayProjectDetails) {
      setShouldDisplayProjectDetails(false);
    }
    searchProjects({
      variables: { search: searchInput.trim(), first: 20 },
    }).then(() => {
      handleClearInput();
    });
  };

  const shouldShowNoContent = [
    !searchResults?.projects?.nodes?.length,
    !loading,
    hasGetProjectsBeenCalled,
  ].every(Boolean);

  return (
    <Container>
      <SearchContainer>
        <StyledHeader>Gitlab Projects</StyledHeader>
        <StyledForm onSubmit={handleSubmit}>
          <input
            name="search"
            value={searchInput}
            onChange={handleChange}
            placeholder="Search Projects"
          />
          <button type="submit">Search</button>
          <button onClick={handleClearInput}>Clear</button>
        </StyledForm>
      </SearchContainer>

      {loading && <Loading />}

      {shouldShowNoContent && <NoContent />}

      {searchResults && !selectedProjectPath && (
        <SearchResults
          searchResults={searchResults}
          selectedProject={(fullPath: string) => {
            setSelectedProjectPath(fullPath);
          }}
        />
      )}

      {selectedProjectPath && (
        <SearchDetailsSection>
          <StyledBackButton onClick={() => setSelectedProjectPath("")}>
            Back
          </StyledBackButton>
          <ProjectDetails fullPath={selectedProjectPath} />
        </SearchDetailsSection>
      )}
    </Container>
  );
};

export default Search;
