import styled from "styled-components";

interface Props {
  className?: string;
}
const Loading = ({ className }: Props) => (
  <h3 className={className}>Loading...</h3>
);

export default styled(Loading)``;
