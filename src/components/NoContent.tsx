import styled from "styled-components";

const StyledNoConent = styled.p`
  font-size: 16px;
  font-weight: 600;
`;
const NoContent = ({ className }: { className?: string }) => {
  return (
    <StyledNoConent className={className}>
      There doesn't seem to be any projects with your search. Please try again.
    </StyledNoConent>
  );
};

export default styled(NoContent)``;
