export const parseMemberNames = (members: { [key: string]: any }) => {
  const memberNames = members?.nodes?.map((member: any) => {
    return member.user.name;
  });
  return memberNames;
};
