import { gql, useLazyQuery, LazyQueryHookOptions } from "@apollo/client";

const PROJECTS = gql`
  query Projects($search: String, $first: Int) {
    projects(search: $search, first: $first) {
      nodes {
        fullPath
        name
        description
      }
    }
  }
`;

const useGetProjects = (options?: LazyQueryHookOptions) => {
  const resp = useLazyQuery(PROJECTS, { ...options });
  return resp;
};

export default useGetProjects;
