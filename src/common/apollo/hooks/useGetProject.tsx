import { gql, LazyQueryHookOptions, useQuery } from "@apollo/client";

const PROJECT = gql`
  query Project($fullPath: ID!) {
    project(fullPath: $fullPath) {
      description
      name
      webUrl
      archived
      createdAt
      projectMembers {
        nodes {
          user {
            name
            publicEmail
          }
        }
      }
    }
  }
`;

const useProject = (options?: LazyQueryHookOptions) => {
  const resp = useQuery(PROJECT, {
    ...options,
  });
  return resp;
};

export default useProject;
