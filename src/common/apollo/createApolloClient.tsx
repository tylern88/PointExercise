import {
  ApolloClient,
  InMemoryCache,
} from "@apollo/client";

const uri = 'https://gitlab.com/api/graphql'

const client = new ApolloClient({
  uri,
  cache: new InMemoryCache()
});

export default client