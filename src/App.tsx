import { ApolloProvider } from "@apollo/client";
import client from "./common/apollo/createApolloClient";
import Search from "./components/Search";
import GlobalStyle from "./styles/GlobalStyle";

function App() {
  return (
    <>
      <GlobalStyle />
      <ApolloProvider client={client}>
        <Search />
      </ApolloProvider>
    </>
  );
}

export default App;
