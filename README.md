# Point Exercise

## Installation

<i> prereqs </i>

Please make sure you have node installed

1. Clone repo
2. run `npm install` in the root of the project
3. run `npm start` to start application
4. visit `localhost:3000` to view app

## Technologies Used

- Typescript React (create-react-app typescript template)
- Apollo GraphQL
- Date-fns
- Styled Components

I used Apollo GraphQL to handle my graphql calls because I think it is easy to setup and go and I am used to their api.

I went with Styled Components for styling the app because I think styled components is a joy to use.

Date-fns was brought in to handle the formatting of dates. Probably didn't need this, but I wasn't sure how extensive I would need to go with formatting and calculating dates.

## Issues

Had I had more time, I would've implemented pagination and used localStorage to hold my app's state. These would make the app feel a lot more sleeker.

Another thing I probably would do differently would be implement some sort of routing to handle data flow from `SearchResults` to `SearchDetails`. For this I would've brought in `React Router`.

As you can tell by the application, I am not a UI/UX developer, but given a design, I am able to implement the application to look however it should.
